import request from '@/utils/request'

export function fetchEventList(query) {
  return request({
    url: '/event',
    method: 'get',
    params: query
  })
}

export function fetchImg(query) {
  return request({
    url: '/file/getImg',
    method: 'get',
    params: query
  })
}

export function fetchEventByCondition(data) {
  return request({
    url: '/event/fetchEvent',
    method: 'post',
    data
  })
}

export function fetchEventStatistics(data) {
  return request({
    url: '/event/fetchEventStatistics',
    method: 'post',
    data
  })
}

export function fetchEventDetail(id) {
  return request({
    url: '/event/' + id,
    method: 'get'
  })
}

export function createEvent(data) {
  return request({
    url: '/event',
    method: 'post',
    data
  })
}

export function updateEvent(data) {
  return request({
    url: '/event',
    method: 'put',
    data
  })
}

export function deleteEvent(id) {
  return request({
    url: '/event/' + id,
    method: 'delete'
  })
}
