import request from '@/utils/request'

export function fetchCountyList(query) {
  return request({
    url: '/county',
    method: 'get',
    params: query
  })
}

export function fetchCountyDetail(id) {
  return request({
    url: '/county/' + id,
    method: 'get'
  })
}

export function createCounty(data) {
  return request({
    url: '/county',
    method: 'post',
    data
  })
}

export function updateCounty(data) {
  return request({
    url: '/county',
    method: 'put',
    data
  })
}

export function deleteCounty(id) {
  return request({
    url: '/county/' + id,
    method: 'delete'
  })
}
