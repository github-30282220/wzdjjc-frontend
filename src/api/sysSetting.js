import request from '@/utils/request'

// 管理处接口
export function fetchAdministrativeList(query) {
  return request({
    url: '/ty/manager',
    method: 'get',
    params: query
  })
}

export function fetchAdministrativeDetail(id) {
  return request({
    url: '/ty/manager/' + id,
    method: 'get'
  })
}

export function createAdministrative(data) {
  return request({
    url: '/ty/manager',
    method: 'post',
    data
  })
}

export function updateAdministrative(data) {
  return request({
    url: '/ty/manager',
    method: 'put',
    data
  })
}

export function deleteAdministrative(id) {
  return request({
    url: '/ty/manager/' + id,
    method: 'delete'
  })
}
// 路线接口=================================================================
export function fetchRouteList(query) {
  return request({
    url: '/ty/line',
    method: 'get',
    params: query
  })
}

export function fetchRouteDetail(id) {
  return request({
    url: '/ty/line/' + id,
    method: 'get'
  })
}

export function createRoute(data) {
  return request({
    url: '/ty/line',
    method: 'post',
    data
  })
}

export function updateRoute(data) {
  return request({
    url: '/ty/line',
    method: 'put',
    data
  })
}

export function deleteRoute(id) {
  return request({
    url: '/ty/line/' + id,
    method: 'delete'
  })
}
// 方向接口===========================================================
export function fetchDirectionList(query) {
  return request({
    url: '/ty/direction',
    method: 'get',
    params: query
  })
}

export function fetchDirectionDetail(id) {
  return request({
    url: '/ty/direction/' + id,
    method: 'get'
  })
}

export function createDirection(data) {
  return request({
    url: '/ty/direction',
    method: 'post',
    data
  })
}

export function updateDirection(data) {
  return request({
    url: '/ty/direction',
    method: 'put',
    data
  })
}

export function deleteDirection(id) {
  return request({
    url: '/ty/direction/' + id,
    method: 'delete'
  })
}
// 县城接口=========================================================
export function fetchCountyList(query) {
  return request({
    url: '/ty/district',
    method: 'get',
    params: query
  })
}

export function fetchCountyDetail(id) {
  return request({
    url: '/ty/district/' + id,
    method: 'get'
  })
}

export function createCounty(data) {
  return request({
    url: '/ty/district',
    method: 'post',
    data
  })
}

export function updateCounty(data) {
  return request({
    url: '/ty/district',
    method: 'put',
    data
  })
}

export function deleteCounty(id) {
  return request({
    url: '/ty/district/' + id,
    method: 'delete'
  })
}
// 乡镇接口=======================================================
export function fetchTownList(query) {
  return request({
    url: '/ty/town',
    method: 'get',
    params: query
  })
}

export function fetchTownDetail(id) {
  return request({
    url: '/ty/town/' + id,
    method: 'get'
  })
}

export function createTown(data) {
  return request({
    url: '/ty/town',
    method: 'post',
    data
  })
}

export function updateTown(data) {
  return request({
    url: '/ty/town',
    method: 'put',
    data
  })
}

export function deleteTown(id) {
  return request({
    url: '/ty/town/' + id,
    method: 'delete'
  })
}
// ===================用户接口============================================================================================
export function fetchUserList(query) {
  return request({
    url: '/sys/admin',
    method: 'get',
    params: query
  })
}

export function fetchUserDetail(id) {
  return request({
    url: '/sys/admin/' + id,
    method: 'get'
  })
}

export function createUser(data) {
  return request({
    url: '/sys/admin',
    method: 'post',
    data
  })
}

export function updateUser(data) {
  return request({
    url: '/sys/admin',
    method: 'put',
    data
  })
}

export function deleteUser(id) {
  return request({
    url: '/sys/admin/' + id,
    method: 'delete'
  })
}

