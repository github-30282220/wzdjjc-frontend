import request from '@/utils/request'

// 事件报警查询接口===================================================================
export function fetchEventList(query) {
  return request({
    url: '/ty/event',
    method: 'get',
    params: query
  })
}

export function fetchEventDetail(id) {
  return request({
    url: '/ty/event/' + id,
    method: 'get'
  })
}

export function createEvent(data) {
  return request({
    url: '/ty/event',
    method: 'post',
    data
  })
}

export function updateEvent(data) {
  return request({
    url: '/ty/event',
    method: 'put',
    data
  })
}

export function deleteEvent(id) {
  return request({
    url: '/ty/event/' + id,
    method: 'delete'
  })
}
