import request from '@/utils/request'

export function fetchTownList(query) {
  return request({
    url: '/town',
    method: 'get',
    params: query
  })
}

export function fetchTownDetail(id) {
  return request({
    url: '/town/' + id,
    method: 'get'
  })
}

export function createTown(data) {
  return request({
    url: '/town',
    method: 'post',
    data
  })
}

export function updateTown(data) {
  return request({
    url: '/town',
    method: 'put',
    data
  })
}

export function deleteTown(id) {
  return request({
    url: '/town/' + id,
    method: 'delete'
  })
}
