import request from '@/utils/request'

export function fetchManagementList(query) {
  return request({
    url: '/management',
    method: 'get',
    params: query
  })
}

export function fetchManagementDetail(id) {
  return request({
    url: '/management/' + id,
    method: 'get'
  })
}

export function createManagement(data) {
  return request({
    url: '/management',
    method: 'post',
    data
  })
}

export function updateManagement(data) {
  return request({
    url: '/management',
    method: 'put',
    data
  })
}

export function deleteManagement(id) {
  return request({
    url: '/management/' + id,
    method: 'delete'
  })
}
