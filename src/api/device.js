import request from '@/utils/request'

export function fetchDeviceList(query) {
  return request({
    url: '/device',
    method: 'get',
    params: query
  })
}

export function fetchDeviceListMapMark(query) {
  return request({
    url: '/device/list2',
    method: 'get',
    params: query
  })
}

export function fetchDeviceDetail(id) {
  return request({
    url: '/device/' + id,
    method: 'get'
  })
}

export function createDevice(data) {
  return request({
    url: '/device',
    method: 'post',
    data
  })
}

export function updateDevice(data) {
  return request({
    url: '/device',
    method: 'put',
    data
  })
}

export function deleteDevice(id) {
  return request({
    url: '/device/' + id,
    method: 'delete'
  })
}
