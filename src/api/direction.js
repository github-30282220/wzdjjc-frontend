import request from '@/utils/request'

export function fetchDirectionList(query) {
  return request({
    url: '/direction',
    method: 'get',
    params: query
  })
}

export function fetchDirectionDetail(id) {
  return request({
    url: '/direction/' + id,
    method: 'get'
  })
}

export function createDirection(data) {
  return request({
    url: '/direction',
    method: 'post',
    data
  })
}

export function updateDirection(data) {
  return request({
    url: '/direction',
    method: 'put',
    data
  })
}

export function deleteDirection(id) {
  return request({
    url: '/direction/' + id,
    method: 'delete'
  })
}
