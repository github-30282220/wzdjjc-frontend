import request from '@/utils/request'

export function fetchRouteList(query) {
  return request({
    url: '/route',
    method: 'get',
    params: query
  })
}

export function fetchRouteDetail(id) {
  return request({
    url: '/route/' + id,
    method: 'get'
  })
}

export function createRoute(data) {
  return request({
    url: '/route',
    method: 'post',
    data
  })
}

export function updateRoute(data) {
  return request({
    url: '/route',
    method: 'put',
    data
  })
}

export function deleteRoute(id) {
  return request({
    url: '/route/' + id,
    method: 'delete'
  })
}
