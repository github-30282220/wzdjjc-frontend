import { fetchDeviceList } from 'api/device'

const device = {
  state: {
    deviceData: ''
  },
  mutations: {
    SET_DEVICE: (state, device) => {
      state.deviceData = device
    }
  },
  actions: {
    GetDevice({ commit }, filterParams) {
      return new Promise((resolve, reject) => {
        fetchDeviceList(filterParams).then(response => {
          const data = response.data.data
          commit('SET_DEVICE', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default device
